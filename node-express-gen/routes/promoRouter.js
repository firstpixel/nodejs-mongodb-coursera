var express  = require('express'),
	bodyParser = require('body-parser');


var router = express.Router();
router.use(bodyParser.json());

router.route('/')
.all(function (req,res,next){
	res.writeHead(200, {'Content-Type':'text/plain'});
	
	next();

})
.get(function (req,res,next) {

	res.end('Will send all the promotions to you!');

})
.post(function (req,res,next) {

	res.end('Will add the promotion:' + req.body.name + ' with details:' + req.body.description);

})
.delete(function (req,res,next) {

	res.end('Will delete all the promotions');

});


router.route('/:id')
.all(function (req,res,next){

	res.writeHead(200, {'Content-Type':'text/plain'});
	
	next();

})
.get(function (req,res,next) {

	res.end('Will send the details of the promotions ' + req.params.id + ' to you!');

})
.put(function (req,res,next) {

	res.end('Will update the details of the promotions ' + req.params.id + ' to you!');

})
.delete(function (req,res,next) {

	res.end('Will delete the promotion ' + req.params.id + ' to you!');

});
module.exports = router;
	
