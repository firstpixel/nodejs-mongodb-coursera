//curl -X POST -H "Content-Type: application/json" -d '{"name":"testName", "description":"this is the test description "}' http://localhost:3000/dishes/

var express  = require('express'),
	morgan = require('morgan');

var hostname = 'localhost'
var port = 8080;

var app = express();
app.use(morgan('dev'));

//Dishes
var dishRouter = require("./dishRouter.js");
app.use('/dishes',dishRouter);

//Promotions
var promoRouter = require("./promoRouter.js");
app.use('/promotions',promoRouter);

//Leadership
var leaderRouter = require("./leaderRouter.js");
app.use('/leadership',leaderRouter);

app.use(express.static(__dirname + '/public'));
app.listen(port, hostname, function(){
	console.log(`Server running at http://${hostname}:${port}/`);
});