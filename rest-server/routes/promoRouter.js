var express  = require('express'),
	bodyParser = require('body-parser');

var mongoose = require('mongoose');

var Promotions = require('../models/promotions');

var router = express.Router();
router.use(bodyParser.json());

router.route('/')
.get(function (req,res,next) {

	Promotions.find({}, function(err, item) {
		if(err) throw err;
		res.json(item);
	});


})
.post(function (req,res,next) {

	Promotions.create(req.body, function (err,item) {
		if(err) throw err;

		console.log('Promotion created!');
		var id = item._id;
		res.writeHead(200, {
			'Content-Type' : 'text/plain'
		});
		
		res.end('Added the promotion with id:' + id);

	});
})
.delete(function (req,res,next) {

	Promotions.remove({}, function (err, item) {
		if(err) throw err;
		res.json(item);
	});

});


router.route('/:id')
.get(function (req,res,next) {

	Promotions.findById(req.params.id, function(err, item) {
		if(err) throw err;
		res.json(item);
	});

})
.put(function (req,res,next) {

	Promotions.findByIdAndUpdate(req.params.Id, {
		$set: req.body
	}, { 
		new: true
	}, function(err, item) {
			if(err) throw err;
			res.json(item);
	});

})
.delete(function (req,res,next) {

	Promotions.remove(req.params.id, function (err, resp) {
		if(err) throw err;
		res.json(resp);
	});

});
module.exports = router;
	
