var express  = require('express'),
	bodyParser = require('body-parser');

var mongoose = require('mongoose');

var Leaders = require('../models/leadership');

var Verify = require('./verify');

var leaderRouter = express.Router();
leaderRouter.use(bodyParser.json());

leaderRouter.route('/')
.get(Verify.verifyOrdinaryUser, function (req,res,next) {

	Leaders.find({}, function(err, item) {
		if(err) return next(err);
		res.json(item);
	});


})
.post(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req,res,next) {

	Leaders.create(req.body, function (err,item) {
		if(err) return next(err);

		console.log('Leader created!');
		var id = item._id;
		res.writeHead(200, {
			'Content-Type' : 'text/plain'
		});
		
		res.end('Added the leader with id:' + id);

	});
})
.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req,res,next) {

	Leaders.remove({}, function (err, item) {
		if(err) return next(err);
		res.json(item);
	});

});


leaderRouter.route('/:id')
.get(Verify.verifyOrdinaryUser, function (req,res,next) {

	Leaders.findById(req.params.id, function(err, item) {
		if(err) return next(err);
		res.json(item);
	});

})
.put(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req,res,next) {

	Leaders.findByIdAndUpdate(req.params.id, {
		$set: req.body
	}, { 
		new: true
	}, function(err, item) {
		if(err) return next(err);
		res.json(item);
	});

})
.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req,res,next) {

	Leaders.remove(req.params.id, function (err, resp) {
		if(err) return next(err);
		res.json(resp);
	});

});
module.exports = leaderRouter;