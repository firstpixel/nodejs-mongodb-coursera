var express  = require('express'),
	bodyParser = require('body-parser');

var mongoose = require('mongoose');

var Promotions = require('../models/promotions');

var Verify = require('./verify');

var router = express.Router();
router.use(bodyParser.json());

router.route('/')
.get(Verify.verifyOrdinaryUser, function (req,res,next) {

	Promotions.find({}, function(err, item) {
		if(err) return next(err);
		res.json(item);
	});


})
.post(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req,res,next) {

	Promotions.create(req.body, function (err,item) {
		if(err) return next(err);

		console.log('Promotion created!');
		var id = item._id;
		res.writeHead(200, {
			'Content-Type' : 'text/plain'
		});
		
		res.end('Added the promotion with id:' + id);

	});
})
.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req,res,next) {

	Promotions.remove({}, function (err, item) {
		if(err) return next(err);
		res.json(item);
	});

});


router.route('/:id')
.get(Verify.verifyOrdinaryUser, function (req,res,next) {

	Promotions.findById(req.params.id, function(err, item) {
		if(err) return next(err);
		res.json(item);
	});

})
.put(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req,res,next) {

	Promotions.findByIdAndUpdate(req.params.Id, {
		$set: req.body
	}, { 
		new: true
	}, function(err, item) {
			if(err) return next(err);
			res.json(item);
	});

})
.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req,res,next) {

	Promotions.remove(req.params.id, function (err, resp) {
		if(err) return next(err);
		res.json(resp);
	});

});
module.exports = router;
	
